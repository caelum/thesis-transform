import os
import pickle
from os import listdir
from os.path import isfile, join, isdir
from copy import deepcopy
from datetime import datetime
from decimal import Decimal
from collections import OrderedDict, defaultdict
from nltk.tree import Tree, ParentedTree
from pprint import pprint, pformat
from numpy.random import random_integers
from extended_parented_tree import (
    ExtendedParentedTree, move_to_left_of_parent, move_to_right_of_parent,
    move_to_left_of_given_ancestor, move_to_right_of_given_ancestor,
    find_highest_tree_with_node_as_leftmost, find_highest_tree_with_node_as_rightmost,
    move_to_be_leftmost_child_of_right_sibling, remove_parent_and_move_children_up,
    strip_preterminals_from_positions, strip_subtrees_from_positions, flatten_subtree,
    subtree_only_contains_given_tags, add_parent, convert_to_pygram_node_tree,
    convert_to_zss_node_tree
)
from PyGram import Profile
import subprocess
import re
import sys
import gzip
import logging
import traceback
import compare

now = datetime.now()
now_str = now.strftime('%m%d_%H%M%S')
directory = sys.argv[1]
print '############## %s ##############' % directory
filenames = []
if isdir(directory):
    for f in listdir(directory):
        current_path = join(directory, f)
        if isfile(current_path):
            filenames.append(current_path)
        elif isdir(current_path):
            for g in listdir(current_path):
                new_path = join(current_path, g)
                if isfile(new_path):
                    filenames.append(new_path)
elif isfile(directory):
    filenames = [ directory ]
else:
    logging.error('%s is neither a directory nor a file', directory)
    sys.exit(1)

rule_order = sys.argv[2]

logging.basicConfig(filename='thesis_run_%s_%s.log' % (rule_order, now_str), 
                    level=logging.DEBUG, 
                    format='%(message)s')
                    #format='%(asctime)s - %(levelname)s - %(message)s')


def modified_collapse_unary(tree, w1, w2, collapsePOS=True, collapseRoot=False):
    if collapseRoot == False and isinstance(tree, Tree) and len(tree) == 1:
        node_list = [tree[0]]
    else:
        node_list = [tree]

    found_rule = False
    # depth-first traversal of tree
    while node_list != []:
        node = node_list.pop()
        if isinstance(node, Tree):
            if len(node) == 1 and isinstance(node[0], Tree) and (collapsePOS == True or isinstance(node[0,0], Tree)):
                prev_node = node[0].node
                child_nodes = list()
                for i in range(len(node[0])-1, -1, -1):
                    child_nodes.insert(0, node[0].pop(i))
                node[0:] = child_nodes
                node_list.append(node)
                if (found_rule == False and
                        not (prev_node['rule'].endswith('_C') or prev_node['rule'].endswith('_c'))
                        and (node.node['rule'].endswith('_C') or node.node['rule'].endswith('_c'))):
                    node.node = prev_node
                    found_rule = True
            else:
                found_rule = False
                for child in node:
                    node_list.append(child)
                parent = node
        else:
            found_rule = False


def determine_bracket_style(sentence):
    if sentence.find('(') == -1 and sentence.find(')') == -1:
        return '(', ')'
    elif sentence.find('[') == -1 and sentence.find(']') == -1:
        return '[', ']'
    elif sentence.find('{') == -1 and sentence.find('}') == -1:
        return '{', '}'
    elif sentence.find('<') == -1 and sentence.find('>') == -1:
        return '<', '>'
    else:
        logging.error('Cannot find good bracket style!')
        return None, None


def parse_ptblike_erg_export_into_trees(export_text):
    derivation_lines = list()
    pos_lines = list()

    raw_export_sections = export_text.split('\n\n')
    if len(raw_export_sections) < 4:
        return False, False, None
    export_sections = [x for x in raw_export_sections if x != '']
    # sections are as follows:
    #   0 - notes at top of file
    #   1 - sentence information
    #   2 - derivation tree
    #   3 - pos tree
    beginning_of_sentence = export_sections[1].find('`')
    end_of_sentence = export_sections[1].rfind("'")
    sentence = export_sections[1][beginning_of_sentence+1:end_of_sentence]
    logging.debug('Sentence is "%s"', sentence)
    left_bracket, right_bracket = determine_bracket_style(sentence)
    derivation_tree_lines = []
    pos_tree_str = export_sections[3]
    if pos_tree_str.strip() == '()':
        return False, False, None
    pos_tree_lines = []
    if len(export_sections) == 4:
        derivation_tree_lines = export_sections[2].split('\n')
        pos_tree_lines = pos_tree_str.split('\n')
    else:
        return False, False, None

    sentence_words = list()
    for idx, line in enumerate(derivation_tree_lines):
        words = line.strip().split()
        #logging.debug('%s item line is %s', len(words), line)
        if len(words) < 1:
            continue
        elif len(words) == 1 and words[0].find('"') == -1:
            continue
        elif len(words) == 5 and line.find('"') == -1:
            derivation_lines.append(re.sub('\(\d+ (?P<rule_name>\S+) [0-9\.\-e]+ [0-9]+ [0-9]+', '(\g<rule_name> ', line).strip())
            if left_bracket != '(':
                derivation_lines[-1] = derivation_lines[-1].replace('(', left_bracket)
            #logging.debug('Adding %s', derivation_lines[-1])
        elif len(words) <= 5 and line.find('"') != -1:
            # find leftmost and rightmost "
            left_quote_index = line.find('"')
            right_quote_index = line.rfind('"')
            #derivation_lines.append(re.sub('\("(?P<word>[^"]+)" \d+.*', '\g<word> ', line).strip())
            derivation_lines.append(line[left_quote_index+1:right_quote_index])
            sentence_words.append(line[left_quote_index+1:right_quote_index])
            #logging.debug('Adding %s', derivation_lines[-1])
        else:
            # only bother with this if it has closing parens
            if line.find(')') != -1:
                # find the leftmost rightmost closing paren
                first_found = line.find(')')
                second_found = line.find(')', first_found+1)
                while (second_found != first_found + 1) and (second_found != -1):
                    #logging.debug('first found %s, second found %s', first_found, second_found)
                    first_found = second_found
                    second_found = line.find(')', first_found+1)
                derivation_lines.append(line[first_found:].strip()[:-1])
                #derivation_lines.append(re.sub('[^\)]+(?P<parens>\)+)', '\g<parens>', line).strip()[:-1])
                if right_bracket != ')':
                    derivation_lines[-1] = derivation_lines[-1].replace(')', right_bracket)
                #logging.debug('Adding %s', derivation_lines[-1])
        #logging.debug('derivation lines are %s', derivation_lines)

    #logging.info('Sentence words are %s', sentence_words)
    start_index_from_last = 0
    all_of_pos_tree = ''
    for word in sentence_words:
        start_index = pos_tree_str.find('(%s)' % word, start_index_from_last)
        if start_index == -1:
            logging.error('Unable to find word %s in sentence in POS tree', word)
        word_start_index = start_index + 1
        #logging.debug('Looking at snippet from index %s to %s', start_index_from_last, word_start_index)
        pos_tree_portion = ''
        # deliberately snip off the character before the word, which is a bracket
        pos_tree_portion = pos_tree_str[start_index_from_last:word_start_index-1]
        if left_bracket != '(' or right_bracket != ')':
            pos_tree_portion = pos_tree_portion.replace('(', left_bracket)
            pos_tree_portion = pos_tree_portion.replace(')', right_bracket)
        pos_tree_portion += word
        #logging.debug('Added %s to pos tree', pos_tree_portion)
        all_of_pos_tree += pos_tree_portion
        #logging.debug('POS tree so far is %s', all_of_pos_tree)
        # deliberately snip off the character just after the word, which is a bracket
        start_index_from_last = word_start_index + len(word) + 1
        #logging.debug('New start index is %s', start_index_from_last)
    final_portion = pos_tree_str[start_index_from_last:]
    if left_bracket != '(' or right_bracket != ')':
        final_portion = final_portion.replace('(', left_bracket)
        final_portion = final_portion.replace(')', right_bracket)
    all_of_pos_tree += final_portion
    #logging.debug('Entire POS tree is %s', all_of_pos_tree)

    all_of_derivation = ' '.join(derivation_lines)[:-1]
    #all_of_pos_tree = ''.join(pos_lines)

    if len(all_of_derivation) == 0:
        return False, False, None

    bracket_str = '%s%s' % (left_bracket, right_bracket)
    pos_tree = Tree.parse('%s%s%s' % (left_bracket, all_of_pos_tree, right_bracket), 
                          brackets=bracket_str)
    if len(pos_tree.treepositions()) <= 1:
        return False, False, None
    derivation_tree = Tree.parse('%s%s%s' % (left_bracket, all_of_derivation, right_bracket), 
                                 brackets=bracket_str)

    return derivation_tree, pos_tree, sentence


def combine_pos_tree_into_derivation_tree(derivation_tree, pos_tree):
    #logging.debug('pos tree %s', pos_tree)
    #logging.debug('derivation tree %s', derivation_tree)
    new_tree = derivation_tree.copy(deep=True)
    deriv_positions = derivation_tree.treepositions()
    tree_positions = pos_tree.treepositions()
    #logging.debug('deriv_positions %s', deriv_positions)
    #logging.debug('pos positions %s', tree_positions)
    if deriv_positions == tree_positions:
        for der in deriv_positions:
            if isinstance(new_tree[der], Tree):
                new_tree[der].node = {
                    'pos': pos_tree[der].node,
                    'rule': derivation_tree[der].node
                }
    return new_tree

HAS_OWN_POS_TOKENS = [
    '\xe2\x80\x9d',   # right curved double quote
    '\xe2\x80\x9c',   # left curved double quote
    '\xe2\x80\x93',   # double dash
    '\xe2\x80\x99',   # right curved single quote
    '\xe2\x80\x98',   # left-tilted single quote
    '``',
    "''",
    '#',
    '?',
    '!',
    '\\/',
    '/',
    '\\',
    '{',
    '}',
    '[',
    ']',
    '(',
    ')',
    ',',
    '"',
    ';', 
    '<',
    '>',
    '@',
    '%',
    '&',
    '$',
    ':',
    '--',
    '...',
#    '-',
]

END_OF_WORD_TOKENS = [
    ',',
    '.',
    "'s",
    "'S",
    "'m",
    "'M",
    "'d",
    "'D",
    "'ll",
    "'re",
    "'ve",
    "n't",
    "'LL",
    "'RE",
    "'VE",
    "N'T",
]
new_eow_tokens = list()
for poss_token_needing_quotes in END_OF_WORD_TOKENS:
    token_with_left_curved_quote = poss_token_needing_quotes.replace("'", '\xe2\x80\x98')
    token_with_right_curved_quote = poss_token_needing_quotes.replace("'", '\xe2\x80\x99')
    new_eow_tokens.append(token_with_left_curved_quote)
    new_eow_tokens.append(token_with_right_curved_quote)
END_OF_WORD_TOKENS.extend(new_eow_tokens)

BEGINNING_OF_WORD_TOKENS = [
    '$',
]

# possibly unescape / which has \ before it
POSSIBLE_TOKENS = [
    "'s",
    "'S",
    "'m",
    "'M",
    "'d",
    "'D",
    "'ll",
    "'re",
    "'ve",
    "n't",
    "'LL",
    "'RE",
    "'VE",
    "N'T",
]
new_poss_tokens = list()
for poss_token_needing_quotes in POSSIBLE_TOKENS:
    token_with_left_curved_quote = poss_token_needing_quotes.replace("'", '\xe2\x80\x98')
    token_with_right_curved_quote = poss_token_needing_quotes.replace("'", '\xe2\x80\x99')
    new_poss_tokens.append(token_with_left_curved_quote)
    new_poss_tokens.append(token_with_right_curved_quote)
POSSIBLE_TOKENS.extend(new_poss_tokens)
POSSIBLE_TOKENS.extend(deepcopy(HAS_OWN_POS_TOKENS))

REPLACEMENT_TOKENS = [
    '-LRB-',
    '-RRB-',
    '-LSB-',
    '-RSB-',
    '-LCB-',
    '-RCB-',
    "''",
    "``",
]

BRACKETS = [
    '-LRB-',
    '-RRB-',
    '-LSB-',
    '-RSB-',
    '-LCB-',
    '-RCB-'
]

SPLIT_ON_LAST_WORD_TOKENS = ['...', '.']

def determine_found_token(found_token, add_left, add_right):
    if found_token in ['"', '\xe2\x80\x9c'] and add_left:
        found_token = '``'
    elif found_token in ['"', '\xe2\x80\x9d'] and add_right:
        found_token = "''"
    elif found_token in ['\xe2\x80\x93']: # double dash
        found_token = '--'
    elif found_token == '\xe2\x80\x99': # right curved single quote
        found_token = "'"
    elif found_token == '\xe2\x80\x98': # left-tilted single quote
        found_token = '`'
    elif found_token == '\xe2\x80\x99s':
        found_token = "'s"
    elif found_token == '\xe2\x80\x98s':
        found_token = "'s"
    elif found_token == '(':
        found_token = '-LRB-'
    elif found_token == ')':
        found_token = '-RRB-'
    elif found_token == '[':
        found_token = '-LSB-'
    elif found_token == ']':
        found_token = '-RSB-'
    elif found_token == '{':
        found_token = '-LCB-'
    elif found_token == '}':
        found_token = '-RCB-'
    return found_token


def replace_leaf_chars(original_tree, w1, w2):
    left_replacement_chars = [
        ('"', "``"),
    ]
    right_replacement_chars = [
        ('"', "''"),
    ]
    all_replacement_chars = [
        ('\xe2\x80\x9c', "``"),
        ('\xe2\x80\x9d', "''"),
        ('\xe2\x80\x93', '--'),
        ('\xe2\x80\x93', '--'),
        ('\xe2\x80\x99', "'"),
        ('\xe2\x80\x98', '`'),
        ('\xe2\x80\xa6', '...'),
        ('/', '\\/'),
        ('(', '-LRB-'),
        (')', '-RRB-'),
        ('[', '-LSB-'),
        (']', '-RSB-'),
        ('{', '-LCB-'),
        ('}', '-RCB-'),
    ]
    positions = original_tree.treepositions(order='leaves')
    for leaf_pos in positions:
        word = original_tree[leaf_pos]
        for (char_to_replace, replacement_char) in all_replacement_chars:
            word = re.sub(re.escape(char_to_replace), replacement_char, word)
        for (char_to_replace, replacement_char) in left_replacement_chars:
            if word[0] == char_to_replace:
                word[0] = replacement_char
        for (char_to_replace, replacement_char) in right_replacement_chars:
            if word[-1] == char_to_replace:
                word[-1] = replacement_char
        original_tree[leaf_pos] = word

current_identifier = None

deepbank_combined_nodes = defaultdict(list)
def _recombine_nodes(new_tree, start_position, end_position, combined_words):
    words = new_tree.leaves()[start_position:end_position+1]
    position_spanning_leaves = new_tree.treeposition_spanning_leaves(start_position, end_position+1)
    spanning_tree = new_tree[position_spanning_leaves]
    #logging.debug('original subtree: %s', spanning_tree.pprint_latex_qtree())
    is_str_list = True
    for i in spanning_tree:
        if not isinstance(i, basestring):
            is_str_list = False
    if is_str_list:
        new_tree[position_spanning_leaves][0:] = [''.join(combined_words)]
    else:
        # make a copy
        new_subtree = spanning_tree.copy(deep=True)
        subtree_leaves = new_subtree.leaves()
        start_idx_in_new = subtree_leaves.index(words[0])
        end_idx_in_new = subtree_leaves.index(words[-1])
        previous_parent = None
        subtree_positions = list()
        for sub_idx in range(start_idx_in_new, end_idx_in_new+1):
            this_parent = new_subtree.leaf_treeposition(sub_idx)[:-1]
            if this_parent != previous_parent:
                subtree_positions.append(new_subtree.leaf_treeposition(sub_idx))
            previous_parent = this_parent
        logging.debug('Subtree POS for "%s" are %s', combined_words, [new_subtree[x[:-1]].node['pos'] for x in subtree_positions])
        deepbank_combined_nodes[current_identifier].append(([new_subtree[x[:-1]].node['pos'] for x in subtree_positions],
                                                            combined_words))
        new_subtree[subtree_positions[-1]] = ''.join(combined_words)
        for sub_treepos in reversed(subtree_positions[:-1]):
            top_node = new_subtree[sub_treepos[:-1]]
            while top_node.parent is not None and top_node.left_sibling is None and top_node.right_sibling is None:
                top_node = top_node.parent
            del new_subtree[top_node.treeposition]
        new_tree[position_spanning_leaves] = new_subtree
    #logging.debug('new subtree: %s', new_tree[position_spanning_leaves].pprint_latex_qtree())
    return new_tree


def reconcile_characters(repp_token, erg_token):
    equivalent_charsets = [
        # x9c are doubled left curved quotes, x9d are doubled right curved quotes
        set(['"', "``", "''", '\xe2\x80\x9c', '\xe2\x80\x9d']),
        set(['--', '-', '\xe2\x80\x93']),
        # x98 are single left curved quotes, x99 are single right curved quotes
        set(["'", "`", '\xe2\x80\x98', '\xe2\x80\x99']),
        set(['...', '\xe2\x80\xa6']),
        set(['/', '\\/']),
    ]
    if erg_token == repp_token:
        return erg_token
    else:
        for equiv_set in equivalent_charsets:
            for repp_char in equiv_set:
                if repp_char in repp_token and repp_char not in erg_token:
                    #logging.debug('Found %s on repp_token', repp_char)
                    for erg_char in equiv_set:
                        if erg_char in erg_token:
                            #logging.debug('Found %s on erg_token', erg_char)
                            corrected_token = erg_token.replace(erg_char, repp_char)
                            #logging.debug('Corrected token is %s', corrected_token)
                            return corrected_token
        return erg_token


def retokenize_tree(original_tree, repp_tokens, w2):
    lower_repp_tokens = [x.lower() for x in repp_tokens]
    logging.debug('repp tokens are %s', lower_repp_tokens)
    erg_tokens = original_tree.leaves()
    lower_erg_tokens = [x.lower() for x in erg_tokens]
    #logging.debug('erg tokens are %s', erg_tokens)
    #logging.debug('repp tokens are %s', repp_tokens)
    #logging.debug('erg tree is %s', original_tree)
    max_tries = 50
    num_tries = 0
    while ((lower_repp_tokens != lower_erg_tokens) and
           (num_tries <= max_tries)):
        num_tries += 1
        #logging.debug('erg tokens: %s', lower_erg_tokens)
        #logging.debug('repp tokens: %s', lower_repp_tokens)
        erg_indices = range(len(lower_erg_tokens))
        repp_indices = range(len(lower_repp_tokens))
        for idx, (erg_idx, repp_idx) in enumerate(map(None, erg_indices, repp_indices)):
            if erg_idx is None:
                last_erg_pos = original_tree.leaf_treeposition(idx-1)
                node_to_copy = original_tree[last_erg_pos[:-1]]
                new_node = node_to_copy.copy(deep=True)
                new_node.node = deepcopy(node_to_copy.node)
                new_node[0:] = [repp_tokens[repp_idx]]
                node_to_copy.add_right_sibling(new_node)
                erg_tokens = original_tree.leaves()
                lower_erg_tokens = [x.lower() for x in erg_tokens]
                break
            repp_token = lower_repp_tokens[repp_idx]
            original_erg_token = lower_erg_tokens[erg_idx]
            erg_token = reconcile_characters(repp_token, original_erg_token)
            #logging.debug('erg token is %s, repp token is %s', erg_token, repp_token)
            if original_erg_token != erg_token:
                #logging.debug('old erg was %s, new erg is %s', original_erg_token, erg_token)
                current_pos = original_tree.leaf_treeposition(idx)
                current_node_and_sibs = original_tree[current_pos[:-1]][0:]
                # replace current node with character-reconciled one
                new_node_and_sibs = list()
                for current_sib in current_node_and_sibs:
                    if current_sib == original_erg_token:
                        new_node_and_sibs.append(erg_token)
                    else:
                        new_node_and_sibs.append(current_sib)
                original_tree[current_pos[:-1]][0:] = new_node_and_sibs
                #logging.debug('erg tree is %s', original_tree)
                #logging.debug('New sibs are %s', original_tree[current_pos[:-1]][0:])
                erg_tokens = original_tree.leaves()
                lower_erg_tokens = [x.lower() for x in erg_tokens]
                erg_token = lower_erg_tokens[idx]

            if erg_token != repp_token:
                len_erg = len(erg_token)
                len_repp = len(repp_token)

                if len_erg > len_repp:
                    if erg_token[:len_repp] == repp_token:
                        left_word = erg_token[:len_repp]
                        right_word = erg_token[len_repp:]
                        erg_pos = original_tree.leaf_treeposition(idx)
                        nth_child_to_edit = erg_pos[-1]
                        node_to_copy = original_tree[erg_pos[:-1]]
                        new_node = node_to_copy.copy(deep=True)
                        new_node.node = deepcopy(node_to_copy.node)
                        new_node[0:] = [right_word]
                        node_to_copy.add_right_sibling(new_node)
                        node_to_copy[nth_child_to_edit] = left_word

                        #if token_is_own_pos:
                        #    if isinstance(new_node.node, str):
                        #        new_node._parent.node['pos'] = found_token
                        #    else:
                        #        new_node.node['pos'] = found_token

                        erg_tokens = original_tree.leaves()
                        lower_erg_tokens = [x.lower() for x in erg_tokens]
                        break
                else:
                    if repp_token[:len_erg] == erg_token:
                        initial_start_idx = idx
                        start_idx = initial_start_idx
                        initial_end_idx = idx + 1
                        end_idx = initial_end_idx
                        combined_words = ''.join([lower_erg_tokens[initial_start_idx],
                                                  lower_erg_tokens[initial_end_idx]])

                        start_pos = original_tree.leaf_treeposition(initial_start_idx)
                        end_pos = original_tree.leaf_treeposition(initial_end_idx)
                        same_parent = bool(start_pos[:-1] == end_pos[:-1])

                        start_node_and_sibs = original_tree[start_pos[:-1]][0:]
                        if len(start_node_and_sibs) > 1:
                            # really want leftmost sibling of this node
                            if same_parent:
                                nth_idx_of_child = start_pos[-1]
                                if nth_idx_of_child != 0:
                                    # if start pos is not child idx 0, make it so
                                    extra_start = nth_idx_of_child
                                    start_idx = initial_start - extra_start
                                    additional_start_words = lower_erg_tokens[start_idx:initial_start_idx]
                                    additional_start_words.append(combined_words)
                                    combined_words = ''.join(additional_start_words)
                            else:
                                extra_start = len(start_node_and_sibs) - 1
                                start_idx = initial_start_idx - extra_start
                                additional_start_words = lower_erg_tokens[start_idx:initial_start_idx]
                                additional_start_words.append(combined_words)
                                combined_words = ' '.join(additional_start_words)

                        end_node_and_sibs = original_tree[end_pos[:-1]][0:]
                        if len(end_node_and_sibs) > 1:
                            if same_parent:
                                nth_idx_of_child = end_pos[-1]
                                max_end = len(end_node_and_sibs) - 1
                                if nth_idx_of_child != max_end:
                                    extra_end = max_end - nth_idx_of_child
                                    end_idx = initial_end_idx + extra_end
                                    additional_end_words = lower_erg_tokens[initial_end_idx+1:end_idx+1]
                                    additional_end_words.insert(0, combined_words)
                                    combined_words = ''.join(additional_end_words)
                            else:
                                extra_end = len(end_node_and_sibs) - 1
                                end_idx = initial_end_idx + extra_end
                                additional_end_words = lower_erg_tokens[initial_end_idx+1:end_idx+1]
                                additional_end_words.insert(0, combined_words)
                                combined_words = ' '.join(additional_end_words)

                        original_tree = _recombine_nodes(original_tree, start_idx, end_idx, combined_words)
                        erg_tokens = original_tree.leaves()
                        lower_erg_tokens = [x.lower() for x in erg_tokens]
                        break
    if lower_repp_tokens != lower_erg_tokens:
        logging.error('Unable to reconcile tokenization!')
        logging.debug('ERG tokens %s', erg_tokens)
        logging.debug('REPP tokens %s', repp_tokens)
    #logging.debug('ERG tree %s', original_tree)


def correct_node_pos(original_tree, w1, w2):
    positions = original_tree.treepositions(order='leaves')
    for leaf_pos in positions:
        word = original_tree[leaf_pos]
        original_pos = original_tree[leaf_pos[:-1]].node['pos']
        token_is_own_pos = word in HAS_OWN_POS_TOKENS or word in SPLIT_ON_LAST_WORD_TOKENS
        if token_is_own_pos:
            #initial round; replacement below also possible
            original_tree[leaf_pos[:-1]].node['pos'] = word
        #overrides
        if word.endswith('ing') and original_pos in ['VP', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']:
            original_tree[leaf_pos[:-1]].node['pos'] = 'VBG'
        elif word in ['--', ';', ':', '...']:
            original_tree[leaf_pos[:-1]].node['pos'] = ':'
        elif word == 'U.S.':
            original_tree[leaf_pos[:-1]].node['pos'] = 'NNP'
        elif word == '`' and original_pos != 'POS':
            original_tree[leaf_pos[:-1]].node['pos'] = '``'
        elif word == "'" and original_pos != 'POS':
            original_tree[leaf_pos[:-1]].node['pos'] = "''"
        elif word == '%':
            original_tree[leaf_pos[:-1]].node['pos'] = 'NN'
        elif word == "n't":
            original_tree[leaf_pos[:-1]].node['pos'] = 'RB'
        elif word == 'to' and original_pos != 'TO':
            if len(positions) < 8:
                logging.debug('Applied TO to shortish sentence')
            original_tree[leaf_pos[:-1]].node['pos'] = 'TO'
        elif word in ['more', 'less']:
            original_tree[leaf_pos[:-1]].node['pos'] = 'RBR'
        elif word == 'which':
            original_tree[leaf_pos[:-1]].node['pos'] = 'WDT'
        elif word == '&':
            original_tree[leaf_pos[:-1]].node['pos'] = 'CC'
        elif word == '-LRB-':
            original_tree[leaf_pos[:-1]].node['pos'] = '-LRB-'
        elif word == '-RRB-':
            original_tree[leaf_pos[:-1]].node['pos'] = '-RRB-'
        elif word == '-LSB-':
            original_tree[leaf_pos[:-1]].node['pos'] = '-LSB-'
        elif word == '-RSB-':
            original_tree[leaf_pos[:-1]].node['pos'] = '-RSB-'
        elif word == '-LCB-':
            original_tree[leaf_pos[:-1]].node['pos'] = '-LCB-'
        elif word == '-RCB-':
            original_tree[leaf_pos[:-1]].node['pos'] = '-RCB-'


def correct_tree_node_pos(original_tree, w1, w2):
    all_positions = sorted(original_tree.treepositions(order='preorder'), key=len)
    leaf_positions = original_tree.treepositions(order='leaves')
    positions_to_examine = strip_preterminals_from_positions(all_positions, leaf_positions)
    for position in positions_to_examine:
        modified = False
        original_pos = original_tree[position].node['pos']
        if original_pos in ['NN', 'NNS', 'NNP', 'NNPS', 'PRP', 'PRP$', 'CD',
                            'POS', 'DT', '$', '.', '``', "''"]:
            original_tree[position].node['pos'] = 'NP'
            modified = True
        elif original_pos in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ', 'MD']:
            original_tree[position].node['pos'] = 'VP'
            modified = True
        elif original_pos in ['IN']:
            original_tree[position].node['pos'] = 'PP'
            modified = True
        elif original_pos in ['RB', 'RBR', 'RBS', 'TO']:
            original_tree[position].node['pos'] = 'ADVP'
            modified = True
        elif original_pos in ['JJ', 'JJR', 'JJS']:
            original_tree[position].node['pos'] = 'ADJP'
            modified = True
        elif original_pos in ['WDT', 'WP', 'WP$']:
            original_tree[position].node['pos'] = 'WHNP'
            modified = True
        elif original_pos in ['WRB']:
            original_tree[position].node['pos'] = 'WHAVP'
            modified = True
        elif original_pos in ['CC']:
            original_tree[position].node['pos'] = 'CONJP'
            modified = True
        elif original_pos in ['UH']:
            original_tree[position].node['pos'] = 'INTJ'
            modified = True
        elif original_pos in ['LS']:
            original_tree[position].node['pos'] = 'LST'
            modified = True
        elif original_pos in ['WRB']:
            original_tree[position].node['pos'] = 'WHAVP'
            modified = True
        elif original_pos in ['EX']:
            original_tree[position].node['pos'] = 'S'
            modified = True
        if modified and len(leaf_positions) < 8:
            logging.debug('Applied correct tree node labels to shortish sentence: %s', len(leaf_positions))


def get_ptb_tree_for_sentence(deepbank_id, deepbank_id_to_ptb_tree):
    output = deepbank_id_to_ptb_tree[deepbank_id]
    ptb_tree = ExtendedParentedTree(output)

    return ptb_tree


def clean_ptb_tree(raw_ptb_tree):
    new_tree = raw_ptb_tree.copy(deep=True)
    leaf_positions = new_tree.treepositions(order='leaves')
    for idx, treepos in reversed(list(enumerate(leaf_positions))):
        word = new_tree[treepos]
        word_parent = new_tree[treepos[:-1]]
        word_pos = word_parent.node
        if word_pos == '-NONE-':
            current_treepos = treepos[:-1]
            while (new_tree[current_treepos].left_sibling is None and 
                    new_tree[current_treepos].right_sibling is None):
                current_treepos = current_treepos[:-1]
            del new_tree[current_treepos]
    return new_tree


def move_punctuation_on_erg_tree(erg_tree, w1, w2):
    # find the other punctuation nodes which should move; punctuation should move to
    # the left or right outside of the phrase structure containing it
    leaves = erg_tree.leaves()
    leaf_positions = erg_tree.treepositions(order='leaves')
    for leaf_idx in reversed(range(len(leaves))):
        #logging.debug('erg_tree is %s', erg_tree)
        leaf_position = leaf_positions[leaf_idx]
        leaf = erg_tree[leaf_position]
        leaf_parent = erg_tree[leaf_position[:-1]]
        leaf_pos = leaf_parent.node['pos']
        #logging.debug('leaf is %s', leaf)
        if leaf_pos in ['``', "''", '.', '?', '!', ',', ':']:
            current_position = leaf_position[:-1]
            #logging.debug('current position: %s', current_position)
            # expects leaf position
            ancestor_as_leftmost = find_highest_tree_with_node_as_leftmost(erg_tree, leaf_position)
            #logging.debug('ancestor as leftmost: %s', ancestor_as_leftmost)
            ancestor_as_rightmost = find_highest_tree_with_node_as_rightmost(erg_tree, leaf_position)
            #logging.debug('ancestor as rightmost: %s', ancestor_as_rightmost)
            if ancestor_as_leftmost != current_position:
                move_to_left_of_given_ancestor(erg_tree, current_position, ancestor_as_leftmost)
            elif ancestor_as_rightmost != current_position:
                move_to_right_of_given_ancestor(erg_tree, current_position, ancestor_as_rightmost)
            leaf_positions = erg_tree.treepositions(order='leaves')
            current_position = leaf_positions[leaf_idx][:-1]
            #logging.debug('current pos is %s, mapping to %s', current_position, erg_tree[current_position])
            #logging.debug('erg_tree is %s', erg_tree)


def move_determiners_on_erg_tree(erg_tree, w1, w2):
    # find the other punctuation nodes which should move; punctuation should move to
    # the left or right outside of the phrase structure containing it
    leaves = erg_tree.leaves()
    leaf_positions = erg_tree.treepositions(order='leaves')
    for leaf_idx in reversed(range(len(leaves))):
        #logging.debug('erg_tree is %s', erg_tree)
        leaf_position = leaf_positions[leaf_idx]
        leaf = erg_tree[leaf_position]
        leaf_parent = erg_tree[leaf_position[:-1]]
        leaf_pos = leaf_parent.node['pos']
        #logging.debug('leaf is %s', leaf)
        if leaf_pos in ['DT']:
            current_position = leaf_position[:-1]
            if (erg_tree[current_position].right_sibling
                    and isinstance(erg_tree[current_position].right_sibling, Tree)
                    and erg_tree[current_position].right_sibling.node['pos']
                        in ['NNP', 'NN', 'NNS', 'NNP', 'NNPS', 'NP', 'NP-SBJ']
                    and isinstance(erg_tree[current_position].right_sibling[0], Tree)):
                move_to_be_leftmost_child_of_right_sibling(erg_tree, current_position)
                leaf_positions = erg_tree.treepositions(order='leaves')
                current_position = leaf_positions[leaf_idx][:-1]
                if len(erg_tree.treepositions(order='leaves')) < 8:
                    logging.debug('Applied det to shortish sentence')
                #logging.debug('current pos is %s, mapping to %s', current_position, erg_tree[current_position])
                #logging.debug('erg_tree is %s', erg_tree)


def alter_sentential_subtrees(erg_tree):
    # for S-like things which are not the first element of the second layer
    new_tree = erg_tree.copy(deep=True)
    all_positions = new_tree.treepositions(order='preorder')
    second_positions = [x for x in sorted(all_positions) if len(x) == 2]
    second_positions = second_positions[1:]
    #logging.debug('erg_tree is %s', erg_tree)
    for second_idx in reversed(range(len(second_positions))):
        second_position = second_positions[second_idx]
        second_node = new_tree[second_position]
        second_pos = second_node.node['pos']
        #logging.debug('leaf is %s', leaf)
        if second_pos in ['S', 'SBAR', 'SBARQ', 'SINV', 'SQ']:
            remove_parent_and_move_children_up(new_tree, second_position)
            all_positions = new_tree.treepositions(order='preorder')
            second_positions = [x for x in sorted(all_positions) if len(x) == 2]
            second_positions = second_positions[1:]
            #logging.debug('current pos is %s, mapping to %s', current_position, new_tree[current_position])
            #logging.debug('new_tree is %s', new_tree)
    return new_tree


def add_initial_npsubj_trees(erg_tree, w1, w2):
    # for S-like things which are not the first element of the second layer
    #logging.debug('new tree: %s', erg_tree)
    nouny_tags = ['CC', 'CD', 'DT', 'NN', 'NNS', 'NNP', 'NNPS', 
                  'NP', 'PRP', 'PRP$']

    positions_to_examine = sorted(erg_tree.treepositions(order='preorder'), key=len)
    while positions_to_examine:
        position_to_check = positions_to_examine.pop()
        if not isinstance(erg_tree[position_to_check], Tree):
            continue
        position_to_check_pos = erg_tree[position_to_check].node['pos']
        if position_to_check_pos not in ['S']:
            continue
        # this is S-like; find children
        children = erg_tree[position_to_check][0:]
        if len(children) < 2:
            continue
        child_poses = [x.node['pos'] for x in children if not isinstance(x, basestring)]
        child_positions = [x.treeposition for x in children]
        if 'VP' in child_poses:
            first_vp_location_idx = child_poses.index('VP')
            first_non_punc_idx = 0
            for idx in range(first_vp_location_idx):
                if child_poses[idx] not in ['"', "''", "'", '``', '`']:
                    first_non_punc_idx = idx
                    break
            if first_vp_location_idx != first_non_punc_idx:
                node_to_examine = erg_tree[child_positions[first_non_punc_idx]]
                #logging.debug('node to examine before is %s', node_to_examine)
                if node_to_examine.node['pos'] in nouny_tags:
                    new_node = node_to_examine.copy(deep=True)
                    new_node.node = deepcopy(node_to_examine.node)
                    node_to_examine[0:] = [new_node]
                    node_to_examine.node['pos'] = 'NP-SBJ'
                    if len(erg_tree.treepositions(order='leaves')) < 8:
                        logging.debug('Applied np-sbj to shortish sentence')
                    positions_to_examine = strip_subtrees_from_positions(positions_to_examine, position_to_check)


def flatten_noun_phrases(erg_tree, w1, w2):
    nouny_tags = ['CC', 'CD', 'DT', 'JJ', 'JJR', 'JJS', 'NN', 'NNS', 'NNP', 'NNPS', 
                  ',', 'NP', 'NP-SBJ', 'RB', 'RBR', 'RBS']
    # go from the top
    all_positions = sorted(erg_tree.treepositions(order='preorder'), key=len)
    leaf_positions = erg_tree.treepositions(order='leaves')
    positions_to_examine = strip_preterminals_from_positions(all_positions, leaf_positions)
    while positions_to_examine:
        position_to_check = positions_to_examine.pop()
        position_pos = erg_tree[position_to_check].node['pos']
        if position_pos in ['NP', 'NP-SBJ']:
            all_nouny = subtree_only_contains_given_tags(erg_tree, position_to_check, nouny_tags)
            if all_nouny:
                #logging.debug('tree is %s', erg_tree)
                #logging.debug('position to check %s, subtree is %s', position_to_check, erg_tree[position_to_check])
                flatten_subtree(erg_tree, position_to_check)
                positions_to_examine = strip_subtrees_from_positions(positions_to_examine, position_to_check)
                if len(leaf_positions) < 8:
                    logging.debug('Applied flatten noun to shortish sentence')


def add_rb_parent_phrases(erg_tree, w1, w2):
    # go from the top
    # TODO exclude fragments
    phrasal_labels = ['S', 'SBAR', 'SBARQ', 'SINV', 'SQ', 'ADJP', 'ADVP', 'CONJP',
                      'FRAG', 'INTJ', 'LST', 'NAC', 'NP', 'NX', 'PP', 'PRN', 'PRT',
                      'QP', 'RRC', 'UCP', 'VP', 'WHADJP', 'WHAVP', 'WHNP', 'WHPP', 'X']
    positions_to_examine = sorted(erg_tree.treepositions(order='preorder'), key=len)
    while positions_to_examine:
        position_to_check = positions_to_examine.pop()
        node_to_examine = erg_tree[position_to_check]
        if isinstance(node_to_examine, Tree):
            position_pos = node_to_examine.node['pos']
            if node_to_examine._parent is None:
                continue
            siblings = node_to_examine._parent[0:]
            sibling_poses = [x.node['pos'] for x in siblings if not isinstance(x, basestring)]
            phrasal_bin = [bool(x in phrasal_labels) for x in sibling_poses]
            phrasal_siblings = any(phrasal_bin)
            if position_pos in ['RB', 'RBR', 'RBS'] and phrasal_siblings:
                node_parent = node_to_examine._parent
                index_on_parent = node_to_examine.parent_index
                parent_pos = node_parent.node['pos']
                if parent_pos not in ['ADVP', 'ADJP']:
                    if parent_pos in ['VP'] and index_on_parent != 0:
                        continue
                    if len(erg_tree.treepositions(order='leaves')) < 8:
                        logging.debug('Applied rb-parent rule to short-ish sentence')
                    #logging.debug('tree is %s', erg_tree)
                    #logging.debug('position to check %s, subtree is %s', position_to_check, erg_tree[position_to_check])
                    new_node = node_to_examine.copy(deep=True)
                    new_node.node = deepcopy(node_to_examine.node)
                    node_to_examine[0:] = [new_node]
                    node_to_examine.node['pos'] = 'ADVP'
                    positions_to_examine = strip_subtrees_from_positions(positions_to_examine, position_to_check)


def move_verb_premodifier_phrases(erg_tree, w1, w2):
    # repeat this until there are none left to do
    premod_phrase_moved = True    # start with true to force at least one run-through
    while(premod_phrase_moved):
        premod_phrase_moved = False
        # go from the top, lop off terminals
        all_positions = set(erg_tree.treepositions(order='preorder'))
        leaf_positions = set(erg_tree.treepositions(order='leaves'))
        these_positions = all_positions - leaf_positions
        orig_positions = sorted(these_positions, key=len)
        cur_len = -1
        temp_pos = list()
        for pos_ex in orig_positions:
            if len(pos_ex) == cur_len:
                temp_pos[-1].append(pos_ex)
            else:
                cur_len = len(pos_ex)
                temp_pos.append(list())
                temp_pos[-1].append(pos_ex)
        positions_to_examine = list()
        for pos_list in temp_pos:
            newlist = pos_list[::-1]
            for pos in newlist:
                positions_to_examine.append(pos)
        while positions_to_examine and not premod_phrase_moved:
            position_to_check = positions_to_examine.pop(0)
            parent_node = erg_tree[position_to_check]
            if (isinstance(parent_node, Tree)
                    and len(parent_node[0:]) > 0
                    and isinstance(parent_node[0], Tree)):
                position_pos = parent_node.node['pos']
                only_child = len(parent_node[0:]) == 1
                if position_pos in ['VP']:
                    leftmost_child = parent_node[0]
                    leftmost_child_pos = leftmost_child.node['pos']
                    if leftmost_child_pos not in ['MD', 'VP', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']:
                        move_to_left_of_parent(erg_tree, leftmost_child.treeposition)
                        premod_phrase_moved = True
                        if only_child:
                            del erg_tree[parent_node.treeposition]
                        if len(erg_tree.treepositions(order='leaves')) < 8:
                            logging.debug('Applied verb premod to shortish sentence')
                        positions_to_examine = strip_subtrees_from_positions(positions_to_examine, position_to_check)


def flatten_sentential_phrases(erg_tree, w1, w2):
    def has_s_child(node):
        for child_idx, child in enumerate(node[0:]):
            if isinstance(child, Tree) and child.node['pos'] in ['S', 'SBAR', 'SBARQ', 'SINV', 'SQ']:
                return child_idx
        return -1

    # find lowest non-branching node
    current_node = erg_tree
    while len(current_node[0:]) == 1:
        if not isinstance(current_node[0], Tree):
            break
        current_node = current_node[0]

    while has_s_child(current_node) != -1:
        child_idx = has_s_child(current_node)
        s_node = current_node[child_idx]
        remove_parent_and_move_children_up(erg_tree, s_node.treeposition)


def move_prepositional_phrases(erg_tree, w1, w2):
    # repeat this until there are none left to do
    prep_phrase_moved = True    # start with true to force at least one run-through
    while(prep_phrase_moved):
        prep_phrase_moved = False
        # go from the top, lop off terminals
        all_positions = set(erg_tree.treepositions(order='preorder'))
        leaf_positions = set(erg_tree.treepositions(order='leaves'))
        these_positions = all_positions - leaf_positions
        orig_positions = sorted(these_positions, key=len)

        cur_len = -1
        temp_pos = list()
        for pos_ex in orig_positions:
            if len(pos_ex) == cur_len:
                temp_pos[-1].append(pos_ex)
            else:
                cur_len = len(pos_ex)
                temp_pos.append(list())
                temp_pos[-1].append(pos_ex)
        positions_to_examine = list()
        for pos_list in temp_pos:
            newlist = pos_list[::-1]
            for pos in newlist:
                positions_to_examine.append(pos)
        while positions_to_examine and not prep_phrase_moved:
            position_to_check = positions_to_examine.pop(0)
            parent_node = erg_tree[position_to_check]
            if (isinstance(parent_node, Tree)
                    and len(parent_node[0:]) > 0
                    and isinstance(parent_node[0], Tree)):
                position_pos = parent_node.node['pos']
                only_child = len(parent_node[0:]) == 1
                #logging.debug('parent in %s', position_pos)
                if position_pos in ['NP', 'NP-SBJ', 'ADJP']:
                    rightmost_child = parent_node[-1]
                    rightmost_child_pos = rightmost_child.node['pos']
                    #logging.debug('rightmost child in %s: %s', rightmost_child_pos, rightmost_child)
                    if rightmost_child_pos in ['PP']:
                        move_to_right_of_parent(erg_tree, rightmost_child.treeposition)
                        prep_phrase_moved = True
                        if only_child:
                            del erg_tree[parent_node.treeposition]
                        if len(erg_tree.treepositions(order='leaves')) < 8:
                            logging.debug('Applied move prep phrases to shortish sentence')
                        positions_to_examine = strip_subtrees_from_positions(positions_to_examine, position_to_check)


def alter_repp_chars(repp_tokenized_words):
    left_replacement_chars = [
        ('"', "``"),
    ]
    right_replacement_chars = [
        ('"', "''"),
    ]
    all_replacement_chars = [
        ('\xe2\x80\x9c', "``"),
        ('\xe2\x80\x9d', "''"),
        ('\xe2\x80\x93', '--'),
        ('\xe2\x80\x93', '--'),
        ('\xe2\x80\x99', "'"),
        ('\xe2\x80\x98', '`'),
        ('\xe2\x80\xa6', '...'),
        ('/', '\\/'),
        ('(', '-LRB-'),
        (')', '-RRB-'),
        ('[', '-LSB-'),
        (']', '-RSB-'),
        ('{', '-LCB-'),
        ('}', '-RCB-'),
    ]
    altered_repp_words = list()
    for word in repp_tokenized_words:
        for (char_to_replace, replacement_char) in all_replacement_chars:
            word = re.sub(re.escape(char_to_replace), replacement_char, word)
        for (char_to_replace, replacement_char) in left_replacement_chars:
            if word[0] == char_to_replace:
                word[0] = replacement_char
        for (char_to_replace, replacement_char) in right_replacement_chars:
            if word[-1] == char_to_replace:
                word[-1] = replacement_char
        altered_repp_words.append(word)
    return altered_repp_words


def preprocess_repp_tokens(repp_tokenized_words):
    all_replacement_chars = [
    #    ('\xe2\x80\x93', '--'),
        ('\xe2\x80\xa6', '...'),
    ]
    altered_repp_words = list()
    for word in repp_tokenized_words:
        for (char_to_replace, replacement_char) in all_replacement_chars:
            word = re.sub(re.escape(char_to_replace), replacement_char, word)
        altered_repp_words.append(word)
    return altered_repp_words


def split_multiword_leaves(original_tree, w1, w2):
    leaf_positions = original_tree.treepositions(order='leaves')
    preterminals = set([x[:-1] for x in leaf_positions])
    nodes_with_leaves = list()
    for tree_pos in reversed(sorted(preterminals)):
        node_to_copy = original_tree[tree_pos]
        children = node_to_copy[0:]
        num_children = len(children)
        if num_children > 1:
            if len(leaf_positions) < 8:
                logging.debug('Applied split multiword to shortish sentence: %s', len(leaf_positions))
            logging.debug('children are %s', children)
            remaining_leaves = list(children)
            first_leaf = remaining_leaves.pop(0)
            node_to_copy[0:] = [first_leaf]
            while remaining_leaves:
                new_leaf = remaining_leaves.pop()
                new_node = node_to_copy.copy(deep=True)
                new_node.node = deepcopy(node_to_copy.node)
                new_node[0:] = [new_leaf]
                node_to_copy.add_right_sibling(new_node)


def rename_nodes(original_tree, w1, safechar_repp_words):
    #logging.debug('Safe REPP words are %s', safechar_repp_words)
    leaf_positions = original_tree.treepositions(order='leaves')
    for tree_pos, new_word in map(None, leaf_positions, safechar_repp_words):
        #logging.debug('treepos is %s, word is %s', tree_pos, new_word)
        if tree_pos != None and new_word != None:
            tree_word = original_tree[tree_pos]
            if tree_word.lower() == new_word.lower():
                original_tree[tree_pos] = new_word


correct_pos = Decimal('0.00')
all_pos = Decimal('0.00')
correct_tree_compare = Decimal('0.00')
all_tree_compare = Decimal('0.00')
correct_tree_structure = Decimal('0.00')
all_tree_structure = Decimal('0.00')
correct_top_one_tree_levels = Decimal('0.00')
all_top_one_tree_levels = Decimal('0.00')
correct_top_two_tree_levels = Decimal('0.00')
all_top_two_tree_levels = Decimal('0.00')
correct_level_by_level = Decimal('0.00')
all_level_by_level = Decimal('0.00')
correct_subtrees = Decimal('0.00')
correct_subtrees_no_func = Decimal('0.00')
all_subtrees = Decimal('0.00')
all_subtrees_no_func = Decimal('0.00')
all_edit_distances = []
all_zss_edit_distances = []

all_files = len(filenames)
file_count = 0
fail_count = Decimal('0.00')
total_fail = Decimal('0.00')
deriv_fail = Decimal('0.00')
pos_fail = Decimal('0.00')

def get_all_ptb_trees():
    deepbank_id_to_ptb_tree = dict()
    # load ptb sentences by identifier
    ptb_tree_file = open('ptb-deepbank-1.0-with-ids.mrg', 'r')
    all_ptb_trees_str = ptb_tree_file.read()
    all_ptb_trees = all_ptb_trees_str.split('\n\n')
    ptb_tree_file.close()
    for ptb_tree_with_id in all_ptb_trees:
        # first line has the id
        tree_lines = ptb_tree_with_id.split('\n')
        id_line = tree_lines[0]
        deepbank_id = id_line[1:].strip()
        ptb_tree = '\n'.join(tree_lines[1:])
        deepbank_id_to_ptb_tree[deepbank_id] = ptb_tree
    return deepbank_id_to_ptb_tree

deepbank_id_to_ptb_tree = get_all_ptb_trees()


def get_all_repp_parses(file_sentence_dict):
    p1 = subprocess.Popen(['cat', 'repp_input.txt'], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(['./logon/bin/cheap', '-t', '-repp', '-preprocess-only=yy',
                           './logon/lingo/erg/english'],
                          stdin=p1.stdout, stdout=subprocess.PIPE, stderr=open(os.devnull, 'wb'))
    p1.stdout.close()
    output, error = p2.communicate()
    output_lines = output.split('\n')
    #logging.debug('output lines: %s', output_lines)
    file_sentence_keys = file_sentence_dict.keys()
    output_lines = output_lines[1:-1]
    ordered_repp_parses = list()
    word_tokens = list()
    first_run = True
    found_idx = 0
    for line in output_lines:
        split_first_part = line.split(',', 5)
        #logging.debug('line %s, second identifier is %s', line, split_first_part[1])
        word_identifier = split_first_part[1].strip()
        split_second_part = split_first_part[-1].rsplit(',', 2)
        unstripped_word = split_second_part[0]
        initial_quote_position = unstripped_word.find('"')
        end_quote_position = unstripped_word.rfind('"')
        stripped_word = unstripped_word[initial_quote_position+1:end_quote_position]
        if int(word_identifier) == 0 and not first_run:
            key = file_sentence_keys[found_idx]
            #logging.debug('key is %s, tokens are %s', key, word_tokens)
            file_sentence_dict[key]['repp_parse'] = deepcopy(word_tokens)
            ordered_repp_parses.append(deepcopy(word_tokens))
            word_tokens = list()
            word_tokens.append(stripped_word)
            found_idx += 1
        else:
            word_tokens.append(stripped_word)
        first_run = False
    ordered_repp_parses.append(word_tokens)
    key = file_sentence_keys[found_idx]
    file_sentence_dict[key]['repp_parse'] = deepcopy(word_tokens)
    #logging.debug('final tokens are %s', word_tokens)

    #logging.debug('Sentence tokenization is %s', word_tokens)
    return file_sentence_dict


def parse_sentence_from_file(export_text):
    raw_export_sections = export_text.split('\n\n')
    if len(raw_export_sections) < 4:
        return False, False, None
    export_sections = [x for x in raw_export_sections if x != '']
    # sections are as follows:
    #   0 - notes at top of file
    #   1 - sentence information
    #   2 - derivation tree
    #   3 - pos tree
    beginning_of_sentence = export_sections[1].find('`')
    end_of_sentence = export_sections[1].rfind("'")
    sentence = export_sections[1][beginning_of_sentence+1:end_of_sentence]
    #logging.debug('Sentence is "%s"', sentence)
    return sentence


def get_all_file_identifiers_and_sentences(filenames):
    identifiers = OrderedDict()
    for fname in filenames:
        file_parts = fname.split('/')
        deepbank_identifier = file_parts[-1]
        identifiers[deepbank_identifier] = {
            'sentence': '',
            'repp_parse': [],
        }
        export_file = open(fname, 'r')
        all_lines_text = export_file.read()
        export_file.close()
        identifiers[deepbank_identifier]['sentence'] = parse_sentence_from_file(all_lines_text)
    return identifiers


if not os.path.exists('repp_output.pkl'):
    repp_input_file = open('repp_input.txt', 'w')
    file_sentences = get_all_file_identifiers_and_sentences(filenames)
    for file_sentence in file_sentences:
        repp_input_file.write('%s\n' % file_sentences[file_sentence]['sentence'])
    repp_input_file.close()

    file_sentences = get_all_repp_parses(file_sentences)
    repp_output_file = open('repp_output.pkl', 'wb')
    pickle.dump(file_sentences, repp_output_file)
    repp_output_file.close()

repp_pickle_input = open('repp_output.pkl', 'rb')
file_sentences = pickle.load(repp_pickle_input)
repp_pickle_input.close()
#logging.debug('file sentences are %s', file_sentences)

output_data = open('ptblike_erg_%s_%s.mrg' % (rule_order, now_str), 'w')

entirely_correct_sentences = Decimal('0.0')
parsed_sentences = Decimal('0.0')

fine_tuned_accuracies = list()
fine_tuned_accuracies_no_func = list()
sentences_by_accuracy = defaultdict(Decimal)
pq_distance_by_sentence_length = defaultdict(list)
sentences_by_accuracy_no_func = defaultdict(Decimal)
pq_distance_by_sentence_length_no_func = defaultdict(list)
all_edit_distances_no_func = []

correct_tokenization = Decimal('0.0')

def store_accuracy(this_accuracy, accuracy_storage):
    if this_accuracy == 1.0:
        accuracy_storage['100'] += 1
    elif this_accuracy >= 0.95:
        accuracy_storage['95-99'] += 1
    elif this_accuracy >= 0.9:
        accuracy_storage['90-94'] += 1
    elif this_accuracy >= 0.85:
        accuracy_storage['85-89'] += 1
    elif this_accuracy >= 0.8:
        accuracy_storage['80-84'] += 1
    elif this_accuracy >= 0.75:
        accuracy_storage['75-79'] += 1
    elif this_accuracy >= 0.7:
        accuracy_storage['70-74'] += 1
    elif this_accuracy >= 0.65:
        accuracy_storage['65-69'] += 1
    elif this_accuracy >= 0.6:
        accuracy_storage['60-64'] += 1
    elif this_accuracy >= 0.55:
        accuracy_storage['55-59'] += 1
    elif this_accuracy >= 0.5:
        accuracy_storage['50-54'] += 1
    elif this_accuracy >= 0.45:
        accuracy_storage['45-49'] += 1
    elif this_accuracy >= 0.4:
        accuracy_storage['40-44'] += 1
    elif this_accuracy >= 0.35:
        accuracy_storage['35-39'] += 1
    elif this_accuracy >= 0.3:
        accuracy_storage['30-34'] += 1
    elif this_accuracy >= 0.25:
        accuracy_storage['25-29'] += 1
    elif this_accuracy >= 0.2:
        accuracy_storage['20-24'] += 1
    elif this_accuracy >= 0.15:
        accuracy_storage['15-19'] += 1
    elif this_accuracy >= 0.1:
        accuracy_storage['10-14'] += 1
    elif this_accuracy >= 0.05:
        accuracy_storage['05-09'] += 1
    else:
        accuracy_storage['0-5'] += 1


def clean_function_tags(tree):
    #logging.debug('Tree is %s', tree)
    all_positions = tree.treepositions(order='preorder')
    for position in all_positions:
        tree_node = tree[position]
        if isinstance(tree_node, basestring):
            continue
        current_pos = tree_node.node
        # exclude the -RCB- type labels
        if current_pos[1:-1].find('-') != -1:
            current_pos = current_pos.split('-')[0]
        tree[position].node = current_pos
    #logging.debug('Tree is %s', tree)


ID_TO_FUNCTION_MAP = {
    'A': split_multiword_leaves,
    'B': retokenize_tree,
    'C': rename_nodes,
    'D': replace_leaf_chars,
    'E': correct_node_pos,
    'F': move_punctuation_on_erg_tree,
    'G': move_determiners_on_erg_tree,
    'H': flatten_sentential_phrases,
    'I': flatten_noun_phrases,
    'J': move_verb_premodifier_phrases,
    'K': modified_collapse_unary,
    'L': add_rb_parent_phrases,
    'M': add_initial_npsubj_trees,
    'N': move_prepositional_phrases,
    'O': correct_tree_node_pos,
}

ptb_sentences_for_combined_nodes = defaultdict(list)

chosen_filenames = list()
#if not os.path.exists('chosen_filenames.pkl'):
#    chosen_indices = list()
#    orig_num_files = len(filenames)
#    while len(chosen_indices) < 100:
#        new_index = random_integers(0, orig_num_files)
#        if new_index not in chosen_indices:
#            chosen_indices.append(new_index)
#
#    for chosen_idx in chosen_indices:
#        chosen_filenames.append(filenames[chosen_idx])
#
#    chosen_filenames_file = open('chosen_filenames.pkl', 'wb')
#    pickle.dump(chosen_filenames, chosen_filenames_file)
#    chosen_filenames_file.close()
#
#chosen_filenames_input = open('chosen_filenames.pkl', 'rb')
#chosen_filenames = pickle.load(chosen_filenames_input)
#chosen_filenames_input.close()
chosen_filenames = filenames

num_files = len(chosen_filenames)
for idx, filename in enumerate(chosen_filenames):
    try:
        #print 'Processing %s of %s: %s%%' % (idx+1, num_files, (float(idx+1)/num_files)*100)
        logging.info('###################################################################################')
        logging.info('###################################################################################')
        logging.info('Processing %s: %s (%s failed all, %s failed pos, %s failed deriv) (%s unparseable) of %s (#%s)',
                     filename, file_count+1, fail_count, total_fail, pos_fail, deriv_fail, all_files, idx)
        export_file = open(filename, 'r')
        all_lines_text = export_file.read()
        export_file.close()

        file_parts = filename.split('/')
        deepbank_identifier = file_parts[-1]
        current_identifier = deepbank_identifier

        derivation_tree, pos_tree, sentence = parse_ptblike_erg_export_into_trees(all_lines_text)
        fail = False
        if not (derivation_tree and pos_tree):
            logging.error('PTB-like ERG was completely unable to parse sentence for derivation and POS')
            total_fail += 1
            fail = True
        if (not derivation_tree) or len(derivation_tree.treepositions()) <= 1:
            logging.error('PTB-like ERG was unable to parse sentence for derivation')
            deriv_fail += 1
            fail = True
        if (not pos_tree) or len(pos_tree.treepositions()) <= 1:
            logging.error('PTB-like ERG was unable to parse sentence for POS')
            pos_fail += 1
            fail = True
        if fail:
            continue
        #logging.debug('file sentences: %s, deepbank identifier %s', file_sentences, deepbank_identifier)
        repp_tokenized_words = file_sentences[deepbank_identifier]['repp_parse']
        #logging.debug('repp words: %s', repp_tokenized_words)
        preprocessed_repp_words = preprocess_repp_tokens(repp_tokenized_words)
        safechar_repp_words = alter_repp_chars(repp_tokenized_words)
        # ExtendedParentedTree.convert(combined_tree)
#        logging.debug('Derivation tree: %s', derivation_tree.pprint_latex_qtree())
#        logging.debug('POS tree: %s', pos_tree.pprint_latex_qtree())
        combined_tree = combine_pos_tree_into_derivation_tree(derivation_tree, pos_tree)
        #logging.debug('Combined tree: %s', combined_tree.pprint_latex_qtree())
        current_tree = ExtendedParentedTree.convert(combined_tree)
        #combined_tree.draw()
        parsed_sentences += 1

        # ABCDEFGJIHKLMO
        for id_letter in rule_order:
            func = ID_TO_FUNCTION_MAP[id_letter]
            func(current_tree, preprocessed_repp_words, safechar_repp_words)
            #logging.debug('Tree after %s: %s', func, current_tree.pprint_latex_qtree())
#        split_multiword_leaves(current_tree, preprocessed_repp_words, safechar_repp_words)        # A --
#        retokenize_tree(current_tree, preprocessed_repp_words, safechar_repp_words)               # B --
#        rename_nodes(current_tree, preprocessed_repp_words, safechar_repp_words)                  # C --
#        replace_leaf_chars(current_tree, preprocessed_repp_words, safechar_repp_words)            # D --
#        correct_node_pos(current_tree, preprocessed_repp_words, safechar_repp_words)              # E --
#        move_punctuation_on_erg_tree(current_tree, preprocessed_repp_words, safechar_repp_words)  # F --
#        move_determiners_on_erg_tree(current_tree, preprocessed_repp_words, safechar_repp_words)  # G --
#        move_verb_premodifier_phrases(current_tree, preprocessed_repp_words, safechar_repp_words) # J --
#        flatten_noun_phrases(current_tree, preprocessed_repp_words, safechar_repp_words)          # I --
#        flatten_sentential_phrases(current_tree, preprocessed_repp_words, safechar_repp_words)    # H --
#        #move_prepositional_phrases(current_tree, preprocessed_repp_words, safechar_repp_words)   # N --
#        modified_collapse_unary(current_tree, preprocessed_repp_words, safechar_repp_words)       # K --
#        add_rb_parent_phrases(current_tree, preprocessed_repp_words, safechar_repp_words)         # L --
#        add_initial_npsubj_trees(current_tree, preprocessed_repp_words, safechar_repp_words)      # M --
#        #correct_node_pos(current_tree, preprocessed_repp_words, safechar_repp_words)              # E --
#        correct_tree_node_pos(current_tree, preprocessed_repp_words, safechar_repp_words)         # O --

#        logging.debug('split_multiword_leaves: %s', current_tree.pprint_latex_qtree())
#        logging.debug('retokenize_tree: %s', current_tree.pprint_latex_qtree())
#        logging.debug('rename_nodes: %s', current_tree.pprint_latex_qtree())
#        logging.debug('replace_leaf_chars: %s', current_tree.pprint_latex_qtree())
#        logging.debug('correct_node_pos: %s', current_tree.pprint_latex_qtree())
#        logging.debug('move_punctuation_on_erg_tree: %s', current_tree.pprint_latex_qtree())
#        logging.debug('move_determiners_on_erg_tree: %s', current_tree.pprint_latex_qtree())
#        logging.debug('flatten_sentential_phrases: %s', current_tree.pprint_latex_qtree())
#        logging.debug('flatten_noun_phrases: %s', current_tree.pprint_latex_qtree())
#        logging.debug('move_verb_premodifier_phrases: %s', current_tree.pprint_latex_qtree())
#        logging.debug('move_prepositional_phrases: %s', current_tree.pprint_latex_qtree())
#        logging.debug('modified_collapse_unary: %s', current_tree.pprint_latex_qtree())
#        logging.debug('add_rb_parent_phrases: %s', current_tree.pprint_latex_qtree())
#        logging.debug('add_initial_npsubj_trees: %s', current_tree.pprint_latex_qtree())
#        logging.debug('correct_node_pos: %s', current_tree.pprint_latex_qtree())
#        logging.debug('correct_tree_node_pos: %s', current_tree.pprint_latex_qtree())

        #sentenced_tree = alter_sentential_subtrees(determined_tree)
        logging.debug('Modified DeepBank tree is %s', current_tree.pprint_latex_qtree())

        output_data.write('#%s\n' % deepbank_identifier)
        output_data.write('%s\n\n' % current_tree)

        ptblike_erg_pos_list = list()
        for deriv_pos in current_tree.pos():
            ptblike_erg_pos_list.append((deriv_pos[0], deriv_pos[1]['pos']))

        parsed_leaves = current_tree.leaves()
        for (tree_word, repp_word) in map(None, parsed_leaves, safechar_repp_words):
            if tree_word != repp_word:
                logging.debug('REPP tokenization is different: tree has %s vs REPP has %s',
                              tree_word, repp_word)
        sentence = ' '.join(parsed_leaves)

        raw_ptb_tree = get_ptb_tree_for_sentence(deepbank_identifier, deepbank_id_to_ptb_tree)
        ptb_tree = clean_ptb_tree(raw_ptb_tree)
        ptb_tree_list = ptb_tree.pos()
        if len(ptblike_erg_pos_list) != len(ptb_tree_list):
            logging.error('Unable to parse differently lengthed sentence: %s', 
                          pformat(map(None, ptblike_erg_pos_list, ptb_tree_list)))
        else:
            correct_tokenization += 1

        logging.debug('PTB tree is %s', ptb_tree.pprint_latex_qtree())

        if current_identifier in deepbank_combined_nodes:
            ptb_terminals = ptb_tree.treepositions(order='leaves')
            for leaf_pos in ptb_terminals:
                leaf = ptb_tree[leaf_pos]
                label = ptb_tree[leaf_pos[:-1]].node
                ptb_sentences_for_combined_nodes[current_identifier].append((label, leaf))

        for ptblike_erg_pos_tuple, ptb_pos_tuple in zip(ptblike_erg_pos_list, ptb_tree_list):
            ptblike_erg_word = ptblike_erg_pos_tuple[0]
            ptblike_erg_pos = ptblike_erg_pos_tuple[1]
            ptb_word = ptb_pos_tuple[0]
            ptb_pos = ptb_pos_tuple[1]
            lower_ptblike_erg_word = ptblike_erg_word.lower()
            lower_ptb_word = ptb_word.lower()
            # TODO need to handle that these words are lowercase with the ptblike erg, when they shouldn't be
            if (lower_ptblike_erg_word.find(lower_ptb_word) == -1
                    and lower_ptblike_erg_word.lower() != 'u.s'
                    and lower_ptb_word.lower() != 'u.s.'):
                logging.warning('Unable to compare words %s vs %s', repr(ptblike_erg_word), repr(ptb_word))
                all_pos += 1
            else:
                all_pos += 1
                if ptblike_erg_pos == ptb_pos:
                    correct_pos += 1
                else:
                    logging.warning('Incorrect POS for "%s" in sentence "%s": erglike "%s" ptb "%s"', 
                                    ptblike_erg_word, sentence, ptblike_erg_pos, ptb_pos)

        # compare in meantime by preprocessing ptb trees and remove trace terminals and delete thing immediately above it
        # look for things that don't belong in the string (known from tokenization)
        
        ptb_tree_positions = ptb_tree.treepositions()
        db_tree_positions = current_tree.treepositions()
        ptb_treepos_set = set(ptb_tree_positions)
        db_treepos_set = set(db_tree_positions)
        common_positions = ptb_treepos_set.intersection(db_treepos_set)
        different_positions = ptb_treepos_set.symmetric_difference(db_treepos_set)
        all_tree_compare += len(common_positions) + len(different_positions)
        all_tree_structure += len(common_positions) + len(different_positions)
        correct_tree_structure += len(common_positions)
        #logging.debug('ERG root %s, PTB root %s', current_tree.root, ptb_tree.root)
        for position in common_positions:
            if len(position) < 3:
                if len(position) < 2:
                    all_top_one_tree_levels += 1
                all_top_two_tree_levels += 1
            ptb_node = ptb_tree[position]
            db_node = current_tree[position]
            ptb_node_label = ptb_node if isinstance(ptb_node, basestring) else ptb_node.node
            db_node_label = db_node if isinstance(db_node, basestring) else db_node.node
            db_pos_tag = db_node_label['pos'] if isinstance(db_node_label, dict) else db_node_label
            if ptb_node_label == db_pos_tag:
                if len(position) < 3:
                    if len(position) < 2:
                        correct_top_one_tree_levels += 1
                    correct_top_two_tree_levels += 1
                correct_tree_compare += 1
            else:
                logging.warning('Incorrect POS for position %s: erglike %s ptb %s',
                                position, db_pos_tag, ptb_node_label)

        len_sorted_ptb_positions = defaultdict(list)
        for position in ptb_tree_positions:
            keylen = len(position)
            len_sorted_ptb_positions[keylen].append(position)
        len_sorted_erg_positions = defaultdict(list)
        for position in db_tree_positions:
            keylen = len(position)
            len_sorted_erg_positions[keylen].append(position)
        max_ptb_len = max(len_sorted_ptb_positions.keys())
        max_erg_len = max(len_sorted_erg_positions.keys())
        min_level = min(max_ptb_len, max_erg_len)
        for i in range(min_level):
            len_i_ptb_pos = set(len_sorted_ptb_positions[i])
            len_i_erg_pos = set(len_sorted_erg_positions[i])
            common_positions = len_i_ptb_pos.intersection(len_i_erg_pos)
            different_positions = len_i_ptb_pos.symmetric_difference(len_i_erg_pos)
            all_level_by_level += len(common_positions) + len(different_positions)
            for position in common_positions:
                ptb_node = ptb_tree[position]
                db_node = current_tree[position]
                ptb_node_label = ptb_node if isinstance(ptb_node, basestring) else ptb_node.node
                db_node_label = db_node if isinstance(db_node, basestring) else db_node.node
                ptb_pos_tag = ptb_node_label['pos'] if isinstance(ptb_node_label, dict) else ptb_node_label
                db_pos_tag = db_node_label['pos'] if isinstance(db_node_label, dict) else db_node_label
                if ptb_pos_tag == db_pos_tag:
                    correct_level_by_level += 1
                else:
                    logging.debug('Incorrect POS at level %s: erglike %s ptb %s',
                                  i, db_pos_tag, ptb_pos_tag)

        converted_erg_tree = ExtendedParentedTree('%s' % current_tree)
        converted_ptb_tree = ExtendedParentedTree('%s' % ptb_tree)
        #logging.debug('PTB tree is %s', ptb_tree)
        converted_erg_tree_no_func = ExtendedParentedTree('%s' % current_tree)
        converted_ptb_tree_no_func = ExtendedParentedTree('%s' % ptb_tree)
        clean_function_tags(converted_erg_tree_no_func)
        clean_function_tags(converted_ptb_tree_no_func)
        tree_setups = [
            (converted_erg_tree, converted_ptb_tree, True),
            (converted_erg_tree_no_func, converted_ptb_tree_no_func, False),
        ]
        for cur_erg_tree, cur_ptb_tree, has_func in tree_setups:
            all_erg_subtrees = [x for x in cur_erg_tree.subtrees()]
            all_ptb_subtrees = [x for x in cur_ptb_tree.subtrees()]
            max_len = max(len(all_ptb_subtrees), len(all_erg_subtrees))
            common_subtrees = [x for x in all_ptb_subtrees if x in all_erg_subtrees]
            if has_func:
                correct_subtrees += len(common_subtrees)
                all_subtrees += max_len
            else:
                correct_subtrees_no_func += len(common_subtrees)
                all_subtrees_no_func += max_len
            this_accuracy = len(common_subtrees)/float(max_len)
            if this_accuracy > 0.90:
                logging.debug('THIS SENTENCE REALLY ACCURATE: %s', this_accuracy)
            if has_func:
                store_accuracy(this_accuracy, sentences_by_accuracy)
                fine_tuned_accuracies.append(this_accuracy)
            else:
                store_accuracy(this_accuracy, sentences_by_accuracy_no_func)
                fine_tuned_accuracies_no_func.append(this_accuracy)

            sentence_length = len(converted_ptb_tree.leaves())
            node_ptb_tree = convert_to_pygram_node_tree(cur_ptb_tree)
            node_erg_tree = convert_to_pygram_node_tree(cur_erg_tree)
            ptb_profile = Profile(node_ptb_tree)
            erg_profile = Profile(node_erg_tree)
            distance = erg_profile.edit_distance(ptb_profile)
            if has_func:
                all_edit_distances.append(distance)
                pq_distance_by_sentence_length[sentence_length].append(distance)
            else:
                all_edit_distances_no_func.append(distance)
                pq_distance_by_sentence_length_no_func[sentence_length].append(distance)
        
#        node_zss_ptb_tree = convert_to_zss_node_tree(converted_ptb_tree)
#        node_zss_erg_tree = convert_to_zss_node_tree(converted_erg_tree)
#        all_zss_edit_distances.append(compare.distance(node_zss_ptb_tree, node_zss_erg_tree))

        #logging.debug('PTB tree is %s', ptb_tree)
        #logging.debug('PTB node tree is %s', node_ptb_tree.pprint())
        #logging.debug('ERG tree is %s', converted_erg_tree)
        #logging.debug('ERG node tree is %s', node_erg_tree.pprint()) 
        #logging.debug('DeepBank tree is %s', current_tree)

        file_count += 1
    except Exception as e:
        logging.exception('Exception when parsing %s', filename)
        fail_count += 1

output_data.close()

if all_pos > 0:
    logging.info('Total POS accuracy %%: %s', str((correct_pos/all_pos)*100))
    print 'Total POS accuracy %%: %s' % str((correct_pos/all_pos)*100)
if all_tree_compare > 0:
    logging.info('Total tree POS accuracy %%: %s', str((correct_tree_compare/all_tree_compare)*100))
    print 'Total tree POS accuracy %%: %s' % str((correct_tree_compare/all_tree_compare)*100)
if all_tree_structure > 0:
    logging.info('Total tree structure accuracy %%: %s', str((correct_tree_structure/all_tree_structure)*100))
    print 'Total tree structure accuracy %%: %s' % str((correct_tree_structure/all_tree_structure)*100)
if all_top_one_tree_levels > 0:
    logging.info('Top one layers of tree accuracy %%: %s', str((correct_top_one_tree_levels/all_top_one_tree_levels)*100))
    print 'Top one layers of tree accuracy %%: %s' % str((correct_top_one_tree_levels/all_top_one_tree_levels)*100)
if all_top_two_tree_levels > 0:
    logging.info('Top two layers of tree accuracy %%: %s', str((correct_top_two_tree_levels/all_top_two_tree_levels)*100))
    print 'Top two layers of tree accuracy %%: %s' % str((correct_top_two_tree_levels/all_top_two_tree_levels)*100)
if all_level_by_level > 0:
    logging.info('Layer by layer tree accuracy %%: %s', str((correct_level_by_level/all_level_by_level)*100))
    print 'Layer by layer tree accuracy %%: %s' % str((correct_level_by_level/all_level_by_level)*100)
if all_subtrees > 0:
    logging.info('Subtree accuracy %%: %s', str((correct_subtrees/all_subtrees)*100))
    print 'Subtree accuracy %%: %s' % str((correct_subtrees/all_subtrees)*100)
if all_subtrees_no_func > 0:
    logging.info('Subtree accuracy %% (no function tags): %s', str((correct_subtrees_no_func/all_subtrees_no_func)*100))
    print 'Subtree accuracy %% (no function tags): %s' % str((correct_subtrees_no_func/all_subtrees_no_func)*100)
if len(all_edit_distances) > 0:
    logging.info('Average edit distance: %s', sum(all_edit_distances)/float(len(all_edit_distances)))
    print 'Average edit distance: %s' % str(sum(all_edit_distances)/float(len(all_edit_distances)))
if len(all_edit_distances_no_func) > 0:
    logging.info('Average edit distance (no function tags): %s', sum(all_edit_distances_no_func)/float(len(all_edit_distances_no_func)))
    print 'Average edit distance (no function tags): %s' % str(sum(all_edit_distances_no_func)/float(len(all_edit_distances_no_func)))
if len(all_zss_edit_distances) > 0:
    logging.info('Average ZSS edit distance: %s', sum(all_zss_edit_distances)/float(len(all_zss_edit_distances)))
    print 'Average ZSS edit distance: %s' % str(sum(all_zss_edit_distances)/float(len(all_zss_edit_distances)))

logging.info('Number of files: %s', num_files)
print 'Number of files: %s' % num_files
logging.info('Both derivation tree and pos tree failed (%s): %s', total_fail, str((total_fail/num_files)*100))
print 'Both derivation tree and pos tree failed (%s): %s' % (total_fail, str((total_fail/num_files)*100))
logging.info('Derivation tree failed (%s): %s', deriv_fail, str((deriv_fail/num_files)*100))
print 'Derivation tree failed (%s): %s' % (deriv_fail, str((deriv_fail/num_files)*100))
logging.info('POS tree failed (%s): %s', pos_fail, str((pos_fail/num_files)*100))
print 'POS tree failed (%s): %s' % (pos_fail, str((pos_fail/num_files)*100))
logging.info('Entirely correct sentences: %s (%s%%)', entirely_correct_sentences, str((entirely_correct_sentences/num_files)*100))
print 'Entirely correct sentences: %s (%s%%)' % (entirely_correct_sentences, str((entirely_correct_sentences/num_files)*100))
logging.info('Number of sentences parsed: %s (%s%%)', parsed_sentences, str((parsed_sentences/num_files)*100))
print 'Number of sentences parsed: %s (%s%%)' % (parsed_sentences, str((parsed_sentences/num_files)*100))
logging.info('Sentences with correct tokenization: %s (%s%%)', correct_tokenization, str((correct_tokenization/num_files)*100))
print 'Sentences with correct tokenization: %s (%s%%)' % (correct_tokenization, str((correct_tokenization/num_files)*100))

logging.info('Sentence accuracy breakdown: %s', sentences_by_accuracy)
print 'Sentence accuracy breakdown: %s' % sentences_by_accuracy
logging.info('Sentence accuracy breakdown (no function_tags): %s', sentences_by_accuracy_no_func)
print 'Sentence accuracy breakdown (no function tags): %s' % sentences_by_accuracy_no_func

end = datetime.now()

fine_tuned_accuracies_file = open('fine_tuned_accuracies_%s_%s.pkl' % (rule_order, now_str), 'wb')
pickle.dump(fine_tuned_accuracies, fine_tuned_accuracies_file)
fine_tuned_accuracies_file.close()

fine_tuned_accuracies_nf_file = open('fine_tuned_accuracies_nf_%s_%s.pkl' % (rule_order, now_str), 'wb')
pickle.dump(fine_tuned_accuracies_no_func, fine_tuned_accuracies_nf_file)
fine_tuned_accuracies_nf_file.close()

db_combining_nodes_file = open('deepbank_combined_nodes_%s_%s.pkl' % (rule_order, now_str), 'wb')
pickle.dump(deepbank_combined_nodes, db_combining_nodes_file)
db_combining_nodes_file.close()

ptb_nodes_for_combining_file = open('ptb_nodes_for_combined_%s_%s.pkl' % (rule_order, now_str), 'wb')
pickle.dump(ptb_sentences_for_combined_nodes, ptb_nodes_for_combining_file, pickle.HIGHEST_PROTOCOL)
ptb_nodes_for_combining_file.close()

pq_distance_by_sentence_length_file = open('pq_distance_by_slength_%s_%s.pkl' % (rule_order, now_str), 'wb')
pickle.dump(pq_distance_by_sentence_length, pq_distance_by_sentence_length_file)
pq_distance_by_sentence_length_file.close()

#pq_distance_by_sentence_length_nf_file = open('pq_distance_by_slength_no_func_%s_%s.pkl' % (rule_order, now_str), 'wb')
#pickle.dump(pq_distance_by_sentence_length_no_func, pq_distance_by_sentence_length_nf_file)
#pq_distance_by_sentence_length_nf_file.close()

logging.info('Elapsed time: %s', end - now)
print 'Elapsed time: %s' % (end - now)
