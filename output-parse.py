import nltk
from nltk.tree import MultiParentedTree
import re
import sys


def traverse(t):
    try:
        t.node
    except AttributeError:
        print t,
    else:
        # Now we know that t.node is defined
        print '(', t.node,
        for child in t:
            traverse(child)
        print ')',

filename = sys.argv[1]

f = open(filename, 'r')
all_lines = f.readlines()
corrected_lines = list()

for line in all_lines:
    corrected_lines.append(re.sub('\((?P<word>\S+)\)', '\g<word>', line))

i = 0
for line in corrected_lines:
    if line[0] == '(':
        break
    i += 1

all_of_tree = ''.join(corrected_lines[i:])

tree = MultiParentedTree(all_of_tree)

#print tree.roots
#print tree.leaves()
#print traverse(tree)
tree.collapse_unary(collapsePOS=True, collapseRoot=True)
print tree

#print [t.height() for t in tree]
#for t in tree.subtrees():
#    print '%s' % t
#print tree.draw()
