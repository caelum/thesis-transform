import nltk
from nltk.tree import MultiParentedTree
import re
import sys


def traverse(t):
    try:
        t.node
    except AttributeError:
        print t,
    else:
        # Now we know that t.node is defined
        print '(', t.node,
        for child in t:
            traverse(child)
        print ')',

filename = sys.argv[1]

f = open(filename, 'r')
all_lines = f.readlines()
corrected_lines = list()

for line in all_lines:
    words = line.strip().split()
    if len(words) == 1:
        continue
    elif len(words) == 5:
        corrected_lines.append(re.sub('\(\d+ (?P<word>\S+) [0-9\.\-]+ [0-9]+ [0-9]+', '(\g<word> ', line).strip())
    elif len(words) == 2:
        corrected_lines.append(re.sub('\((?P<word>"\S+") \d+.*', '\g<word> ', line))
    else:
        corrected_lines.append(re.sub('[^\)]+(?P<parens>\)+)', '\g<parens>', line).strip()[:-1])

print corrected_lines

all_of_tree = ''.join(corrected_lines)

tree = MultiParentedTree(all_of_tree[:-1])
print tree

#print tree.roots
#print tree.leaves()
#print traverse(tree)
#tree.collapse_unary(collapsePOS=True, collapseRoot=True)
#print tree

#print [t.height() for t in tree]
#for t in tree.subtrees():
#    print '%s' % t
#print tree.draw()
