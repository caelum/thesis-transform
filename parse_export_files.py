from os import listdir
from os.path import isfile, join, isdir
from copy import deepcopy
from decimal import Decimal
from nltk.tree import Tree, ParentedTree
import subprocess
import re
import sys
import traceback


def modified_collapse_unary(tree, collapsePOS=False, collapseRoot=False):
    if collapseRoot == False and isinstance(tree, Tree) and len(tree) == 1:
        nodeList = [tree[0]]
    else:
        nodeList = [tree]

    found_rule = False
    # depth-first traversal of tree
    while nodeList != []:
        node = nodeList.pop()
        if isinstance(node, Tree):
            if len(node) == 1 and isinstance(node[0], Tree) and (collapsePOS == True or isinstance(node[0,0], Tree)):
                prev_node = node[0].node
                node[0:] = [child for child in node[0]]
                nodeList.append(node)
                if (found_rule == False and
                        not (prev_node['rule'].endswith('_C') or prev_node['rule'].endswith('_c'))
                        and (node.node['rule'].endswith('_C') or node.node['rule'].endswith('_c'))):
                    node.node = prev_node
                    found_rule = True
            else:
                found_rule = False
                for child in node:
                    nodeList.append(child)
                parent = node
        else:
            found_rule = False


def parse_ptblike_erg_export_into_trees(export_lines):
    derivation_lines = list()
    pos_lines = list()

    start_of_tree = 0
    for idx, line in enumerate(export_lines[9:]):
        words = line.strip().split()
        if len(words) == 0:
            start_of_tree = idx + 10
            break
        elif len(words) == 1:
            continue
        elif len(words) == 5:
            derivation_lines.append(re.sub('\(\d+ (?P<word>\S+) [0-9\.\-]+ [0-9]+ [0-9]+', '(\g<word> ', line).strip())
        elif len(words) == 2 or len(words) == 3:
            derivation_lines.append(re.sub('\("(?P<word>[^"]+)" \d+.*', '\g<word> ', line).strip())
        else:
            derivation_lines.append(re.sub('[^\)]+(?P<parens>\)+)', '\g<parens>', line).strip()[:-1])

    for line in export_lines[start_of_tree:]:
        pos_lines.append(re.sub('\((?P<word>[^\(\)]+)\)', '\g<word>', line))

    all_of_derivation = ' '.join(derivation_lines)[:-1]
    all_of_tree = ''.join(pos_lines)

    if len(all_of_derivation) == 0:
        return False, False

    pos_tree = Tree(all_of_tree)
    if len(pos_tree.treepositions()) <= 1:
        return False, False
    derivation_tree = Tree(all_of_derivation)

    return derivation_tree, pos_tree


def combine_pos_tree_into_derivation_tree(derivation_tree, pos_tree):
    new_tree = deepcopy(derivation_tree)
    deriv_positions = derivation_tree.treepositions()
    tree_positions = pos_tree.treepositions()
    if deriv_positions == tree_positions:
        for der in deriv_positions:
            if isinstance(new_tree[der], Tree):
                new_tree[der].node = {
                    'pos': pos_tree[der].node,
                    'rule': derivation_tree[der].node
                }
    #print new_tree
    return new_tree

HAS_OWN_POS_TOKENS = [
    '\xe2\x80\x9d',   # right curved double quote
    '\xe2\x80\x9c',   # left curved double quote
    '\xe2\x80\x93',   # double dash
    '\xe2\x80\x99',   # right curved single quote
    '\xe2\x80\x98',   # left-tilted single quote
    '#',
    '?',
    '!',
    '/',
    '\\',
    '{',
    '}',
    '[',
    ']',
    '(',
    ')',
    ',',
    '"',
    ';', 
    '<',
    '>',
    '@',
    '%',
    '&',
    '$',
    ':',
    '--',
    '...',
    '-',
]

POSSIBLE_TOKENS = deepcopy(HAS_OWN_POS_TOKENS)
# possibly unescape / which has \ before it
POSSIBLE_TOKENS.extend([
    "'s",
    "'S",
    "'m",
    "'M",
    "'d",
    "'D",
    "'ll",
    "'re",
    "'ve",
    "n't",
    "'LL",
    "'RE",
    "'VE",
    "N'T",
])

REPLACEMENT_TOKENS = [
    '-LRB-',
    '-RRB-',
    '-LSB-',
    '-RSB-',
    '-LCB-',
    '-RCB-',
    "''",
    "``",
]

SPLIT_ON_LAST_WORD_TOKENS = ['.']

def retokenize_tree(original_tree):
    new_tree = deepcopy(original_tree)
    leaf_positions = new_tree.treepositions(order='leaves')
    num_words = len(leaf_positions)
    original_last_word = new_tree[leaf_positions[-1]]
    duplicated_words = 0
    last_position = -1
    is_last_word = False
    while True:
        # find last position; start from the next one after that
        if last_position != -1:
            leaf_positions = new_tree.treepositions(order='leaves')
            last_run = leaf_positions.index(last_position)
            last_word = new_tree[leaf_positions[last_run]]
            print 'last word was "%s"; is last word: %s' % (last_word, is_last_word)
            if last_word not in POSSIBLE_TOKENS and last_word not in REPLACEMENT_TOKENS and last_word not in SPLIT_ON_LAST_WORD_TOKENS:
                has_token_still = False
                for token in POSSIBLE_TOKENS:
                    eos_idx = len(last_word) - len(token)
                    found_idx = last_word.find(token)
                    if found_idx != -1 and (found_idx == 0 or found_idx == eos_idx):
                        has_token_still = True
                        print 'found token "%s"' % token
                        break
                if is_last_word:
                    for token in SPLIT_ON_LAST_WORD_TOKENS:
                        eos_idx = len(last_word) - len(token)
                        found_idx = last_word.find(token)
                        if found_idx != -1 and (found_idx == 0 or found_idx == eos_idx):
                            has_token_still = True
                            print 'found token "%s"' % token
                            break
                if has_token_still:
                    print "still has token; using last position"
                    this_index = leaf_positions[last_run]
                else:
                    print "advancing one position, last word: %s" % is_last_word
                    this_index = leaf_positions[last_run+1]
            else:
                print "last word is token; advancing one position"
                try:
                    this_index = leaf_positions[last_run+1]
                except IndexError:
                    break
        else:
            this_index = leaf_positions[0]
        # determine if should mess with word
        add_left = False
        add_right = False
        word = new_tree[this_index]
        found_idx = -1
        found_token = None
        word_after_token_removal = None
        print 'Looking at word %s' % word
        if word == original_last_word:
            is_last_word = True
        any_found = False
        considered_tokens = deepcopy(POSSIBLE_TOKENS)
        if is_last_word:
            considered_tokens.extend(SPLIT_ON_LAST_WORD_TOKENS)
        for token in considered_tokens:
            eos_idx = len(word) - len(token)
            found_idx = word.find(token)
            if word != token and found_idx != -1 and word not in ['-LRB-', '-RRB-', '-LSB-', '-RSB-', '-LCB-', '-RCB-']:
                print 'Found %s at %s on word %s' % (token, found_idx, word)
                if found_idx == 0:
                    found_token = token
                    word_after_token_removal = word.lstrip(found_token)
                    add_left = True
                    any_found = True
                    break
                elif found_idx == eos_idx:
                    found_token = token
                    word_after_token_removal = word.rstrip(found_token)
                    add_right = True
                    any_found = True
                    break
        if is_last_word and not any_found:
            break
        added_idx = None
        token_is_own_pos = found_token in HAS_OWN_POS_TOKENS or found_token in SPLIT_ON_LAST_WORD_TOKENS
        if found_token in ['"', '\xe2\x80\x9c'] and add_left:
            found_token = '``'
        elif found_token in ['"', '\xe2\x80\x9d'] and add_right:
            found_token = "''"
        elif found_token == '(':
            found_token = '-LRB-'
        elif found_token == '(':
            found_token = '-RRB-'
        elif found_token == '[':
            found_token = '-LSB-'
        elif found_token == ']':
            found_token = '-RSB-'
        elif found_token == '{':
            found_token = '-LCB-'
        elif found_token == '}':
            found_token = '-RCB-'
        if add_left:
            new_tree[this_index[:-2]].insert(this_index[-2], deepcopy(new_tree[this_index[:-1]]))
            left_index = list(deepcopy(this_index))
            left_index[-2] -= 1
            left_index = tuple(left_index)
            new_tree[left_index] = word_after_token_removal
            new_tree[this_index] = found_token
            if token_is_own_pos:
                new_tree[this_index[:-1]].node['pos'] = found_token
            added_idx = deepcopy(this_index)
        elif add_right:
            new_tree[this_index[:-2]].insert(this_index[-2]+1, deepcopy(new_tree[this_index[:-1]]))
            constructed_index = list(deepcopy(this_index))
            constructed_index[-2] += 1
            constructed_index = tuple(constructed_index)
            new_tree[constructed_index] = found_token
            new_tree[this_index] = word_after_token_removal
            if token_is_own_pos:
                new_tree[constructed_index[:-1]].node['pos'] = found_token
            prev_idx = list(deepcopy(this_index))
            prev_idx[-2] = max(0, prev_idx[-2]-1)
            prev_idx = tuple(prev_idx)
            added_idx = deepcopy(prev_idx)
            # not quite right..isn't running through more than once
        else:
            added_idx = deepcopy(this_index)
            duplicated_words += 1
        last_position = added_idx

    #print new_tree
    return new_tree


def get_ptb_tree_for_sentence(sentence):
    g = open('temp.txt', 'w')
    g.write('%s\n' % sentence)
    g.close()

    pipe = subprocess.Popen(['java edu.stanford.nlp.parser.lexparser.LexicalizedParser -outputFormat "penn" /home/cae/Thesis/stanford-parser-2012-07-09/edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz temp.txt'], 
                            shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = pipe.communicate()[0]
    ptb_tree = Tree(output)

    return ptb_tree


correct_pos = Decimal('0.00')
all_pos = Decimal('0.00')

directory = sys.argv[1]
filenames = []
if isdir(directory):
    filenames = [ join(directory, f) for f in listdir(directory) if isfile(join(directory, f)) ]
elif isfile(directory):
    filenames = [ directory ]
else:
    print '%s is neither a directory nor a file' % directory
    sys.exit(1)

all_files = len(filenames)
file_count = 0
fail_count = 0
total_fail = 0
deriv_fail = 0
pos_fail = 0

for idx, filename in enumerate(filenames):
    try:
        print 'Processing %s: %s (%s failed all, %s failed pos, %s failed deriv) (%s unparseable) of %s (#%s)' % (filename, file_count+1, fail_count, total_fail, pos_fail, deriv_fail, all_files, idx)
        export_file = open(filename, 'r')
        all_lines = export_file.readlines()
        export_file.close()

        derivation_tree, pos_tree = parse_ptblike_erg_export_into_trees(all_lines)
        if not (derivation_tree and pos_tree):
            print 'PTB-like ERG was completely unable to parse sentence for derivation and POS'
            total_fail += 1
            continue
        if not derivation_tree:
            print 'PTB-like ERG was unable to parse sentence for derivation'
            deriv_fail += 1
            continue
        if not pos_tree:
            print 'PTB-like ERG was unable to parse sentence for POS'
            pos_fail += 1
            continue
        if len(pos_tree.treepositions()) <= 1:
            print 'PTB-like ERG was unable to parse sentence for POS tags'
            legit_fail += 1
            continue
        combined_tree = combine_pos_tree_into_derivation_tree(derivation_tree, pos_tree)
        current_tree = retokenize_tree(combined_tree)
        modified_collapse_unary(current_tree, collapsePOS=True)

        ptblike_erg_pos_list = list()
        for deriv_pos in current_tree.pos():
            ptblike_erg_pos_list.append((deriv_pos[0], deriv_pos[1]['pos']))

        sentence = ' '.join(current_tree.leaves())

        ptb_tree = get_ptb_tree_for_sentence(sentence)
        ptb_tree_list = ptb_tree.pos()

        if len(ptblike_erg_pos_list) != len(ptb_tree_list):
            print 'Unable to parse differently lengthed sentence: %s vs %s' % (ptblike_erg_pos_list, ptb_tree_list)

        for ptblike_erg_pos_tuple, ptb_pos_tuple in zip(ptblike_erg_pos_list, ptb_tree_list):
            ptblike_erg_word = ptblike_erg_pos_tuple[0]
            ptblike_erg_pos = ptblike_erg_pos_tuple[1]
            ptb_word = ptb_pos_tuple[0]
            ptb_pos = ptb_pos_tuple[1]
            if ptblike_erg_word.find(ptb_word) == -1:
                print 'Unable to compare words %s vs %s' % (ptblike_erg_word, ptb_word)
            else:
                all_pos += 1
                if ptblike_erg_pos == ptb_pos:
                    correct_pos += 1
                else:
                    print 'Incorrect POS for "%s" in sentence "%s": erglike "%s" ptb "%s"' % (ptblike_erg_word, sentence, ptblike_erg_pos, ptb_pos)
        file_count += 1
    except Exception as e:
        traceback.print_exc()
        fail_count += 1

if all_pos > 0:
    print 'Total accuracy %%: %s' % str((correct_pos/all_pos)*100)

